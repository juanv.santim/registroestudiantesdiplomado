﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroEstudiantes.Data.Servicios
{
    public class EstudiantesServiceTest : IEstudiantesService
    {
        List<Estudiante> estudiantes;

        public EstudiantesServiceTest()
        {
            estudiantes = new List<Estudiante>();

            estudiantes.Add(new Estudiante { Id = 1, Nombre = "Jose", Apellidos = "Perez", FechaNacimiento = new DateTime(1990, 1, 1), Sexo = "M" });
            estudiantes.Add(new Estudiante { Id = 2, Nombre = "Pedro", Apellidos = "Sanchez", FechaNacimiento = new DateTime(1995, 3, 13), Sexo = "M" });
            estudiantes.Add(new Estudiante { Id = 3, Nombre = "Maria", Apellidos = "Flores", FechaNacimiento = new DateTime(1992, 4, 3), Sexo = "F" });
            estudiantes.Add(new Estudiante { Id = 4, Nombre = "Jose", Apellidos = "Perez", FechaNacimiento = new DateTime(1990, 1, 1), Sexo = "M" });
            estudiantes.Add(new Estudiante { Id = 5, Nombre = "Mario", Apellidos = "Cespedez", FechaNacimiento = new DateTime(1995, 3, 13), Sexo = "M" });
            estudiantes.Add(new Estudiante { Id = 6, Nombre = "Julio", Apellidos = "Alcantara", FechaNacimiento = new DateTime(1992, 4, 3), Sexo = "F" });
        }

        public void Crear(Estudiante estudiante)
        {
            estudiante.Id = this.estudiantes.Max(e => e.Id) + 1;

            this.estudiantes.Add(estudiante);
        }

        public void CrearSp(Estudiante estudiante)
        {
            throw new NotImplementedException();
        }


        public void Eliminar(Estudiante estudiante)
        {
            var estudianteToDelete = GetById(estudiante.Id);
            this.estudiantes.Remove(estudianteToDelete);
        }

        public Estudiante GetById(int Id)
        {
            var lista = Listar();

            var estudianteEncontrado = lista.SingleOrDefault(estudiante => estudiante.Id == Id);

            return estudianteEncontrado;
        }

        public IEnumerable<Estudiante> Listar()
        {
            return estudiantes;
        }

        public void Modificar(Estudiante estudiante)
        {
            var estudianteAModificar = this.estudiantes.Single(e => e.Id == estudiante.Id);

            estudianteAModificar.Nombre = estudiante.Nombre;
            estudianteAModificar.Apellidos = estudiante.Apellidos;
            estudianteAModificar.FechaNacimiento = estudiante.FechaNacimiento;
            estudianteAModificar.Sexo = estudiante.Sexo;
        }
    }
}
