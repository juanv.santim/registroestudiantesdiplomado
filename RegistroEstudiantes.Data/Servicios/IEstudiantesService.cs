﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroEstudiantes.Data.Servicios
{
    public interface IEstudiantesService
    {
        IEnumerable<Estudiante> Listar();
        Estudiante GetById(int Id);
        void Crear(Estudiante estudiante);
        void Modificar(Estudiante estudiante);
        void Eliminar(Estudiante estudiante);
        void CrearSp(Estudiante estudiante);
    }
}
