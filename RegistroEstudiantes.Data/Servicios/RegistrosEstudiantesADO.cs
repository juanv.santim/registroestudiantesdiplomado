﻿using RegistroEstudiantes.Data.Context;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroEstudiantes.Data.Servicios
{
    public class ViPrintAdo
    {
        public string sqlConnectionString { get; set; }
        public string estudiantesConnectionString { get; set; }

        public ViPrintAdo()
        {
            sqlConnectionString = ConfigurationManager.ConnectionStrings["ViPrint"].ConnectionString;
            estudiantesConnectionString = ConfigurationManager.ConnectionStrings["RegistroEstudiantesDB"].ConnectionString;
        }

        public List<Catalogo> GetProductos()
        {
            List<Catalogo> lista = new List<Catalogo>();

            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * from dbo.CatalogoViPrint", conn);

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                
                while (rdr.Read())
                {
                    var producto = new Catalogo
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Categoria = rdr["Categoria"].ToString(),
                        Nombre = rdr["Nombre de artículo"].ToString(),
                    };

                    if (rdr["precio"] == DBNull.Value)
                    {
                        producto.Precio = 0;
                    }
                    else
                    {
                        producto.Precio = Convert.ToDouble(rdr["precio"]);

                    }

                    lista.Add(producto); 
                }

                conn.Close();
            }

            return lista;
        }

        public void InsertarEstudiante(Estudiante estudiante)
        {
            using (var connection = new SqlConnection(estudiantesConnectionString))
            {
                var sqlCommand = new SqlCommand("spInsertarEstudiante", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@nombre", estudiante.Nombre);
                sqlCommand.Parameters.AddWithValue("@apellidos", estudiante.Apellidos);
                sqlCommand.Parameters.AddWithValue("@fechaNacimiento", estudiante.FechaNacimiento);
                sqlCommand.Parameters.AddWithValue("@sexo", estudiante.Sexo);

                SqlParameter estudianteId = new SqlParameter();
                estudianteId.Direction = ParameterDirection.ReturnValue;
                sqlCommand.Parameters.Add(estudianteId);

                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();

                estudiante.Id = Convert.ToInt32(estudianteId.Value);

            }
        }
    }
}
