﻿using RegistroEstudiantes.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroEstudiantes.Data.Servicios
{
    public class RegistroEstudiantesEFService : IEstudiantesService
    {
        private readonly RegistroEstudiantesContext context;

        public RegistroEstudiantesEFService(RegistroEstudiantesContext context)
        {
            this.context = context;
        }

        public void Crear(Estudiante estudiante)
        {
            context.Estudiantes.Add(estudiante);
            context.SaveChanges();
        }

        public void CrearSp(Estudiante estudiante)
        {
            var Id = context.Database.SqlQuery<int>(@"
            DECLARE	@estudianteId Int;
            EXEC @estudianteId = [dbo].[spInsertarEstudiante] @nombre, @apellidos, @fechaNacimiento, @sexo
            select @estudianteId",
            new SqlParameter("@nombre", estudiante.Nombre),
            new SqlParameter("@apellidos", estudiante.Apellidos),
            new SqlParameter("@fechaNacimiento", estudiante.FechaNacimiento),
            new SqlParameter("@sexo", estudiante.Sexo)).ToList();

            estudiante.Id = Id.First();
        }

  
        public void Eliminar(Estudiante estudiante)
        {
            var estudianteToDelete = GetById(estudiante.Id);

            this.context.Estudiantes.Remove(estudianteToDelete);

            context.SaveChanges();
        }

        public Estudiante GetById(int Id)
        {
            return context.Estudiantes.SingleOrDefault(e => e.Id == Id);
        }

        public IEnumerable<Estudiante> Listar()
        {
            return context.Estudiantes.ToList();
        }

        public void Modificar(Estudiante estudiante)
        {
            var estudianteAModificar = this.context.Estudiantes.Single(e => e.Id == estudiante.Id);

            estudianteAModificar.Nombre = estudiante.Nombre;
            estudianteAModificar.Apellidos = estudiante.Apellidos;
            estudianteAModificar.FechaNacimiento = estudiante.FechaNacimiento;
            estudianteAModificar.Sexo = estudiante.Sexo;

            context.SaveChanges();
        }
    }
}
