﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroEstudiantes.Data
{
    [Table("Estudiantes")]
    public class Estudiante
    {
        public int Id { get; set; }

        [MaxLength(100)]
        [MinLength(2, ErrorMessage ="El nombre de debe tener al menos 2 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }

        [MaxLength(100)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El apellido es requerido")]
        public string Apellidos { get; set; }

        [Display(Name = "Fecha de Nacimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaNacimiento { get; set; }

        public int Edad
        {
            get
            {
                return Convert.ToInt32((DateTime.Now - FechaNacimiento).TotalDays / 360);
            }
        }

        [MaxLength(1)]
        public string Sexo { get; set; }

    }
}
