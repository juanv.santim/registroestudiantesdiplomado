﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroEstudiantes.Data.Context
{
    public class TempContext:DbContext
    {
        public TempContext():base("ViPrint")
        {

        }

        public DbSet<Catalogo> catalogos { get; set; }
    }

    [Table("CatalogoViPrint")]
    public class Catalogo
    {
        public int Id { get; set; }

        [Column("Nombre de artículo")]
        public string Nombre { get; set; }
        public string Categoria { get; set; }

        [Column("precio")]
        public Nullable<double> Precio { get; set; }
    }
}
