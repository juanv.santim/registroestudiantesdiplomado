﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroEstudiantes.Data.Context
{
    public class RegistroEstudiantesContext: DbContext
    {
        public RegistroEstudiantesContext():base("RegistroEstudiantesDB")
        {
          
        }

        public DbSet<Estudiante> Estudiantes { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
    }
}
