﻿using RegistroEstudiantes.Data.Context;
using RegistroEstudiantes.Data.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RegistroEstudiante.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        IEstudiantesService service;

        public HomeController(IEstudiantesService service)
        {
            this.service = service;
        }

        public ActionResult Index()
        {
            var lista = this.service.Listar();

            TempContext tempContext = new TempContext();
            ViPrintAdo tempAdo = new ViPrintAdo();

            try
            {
                //var productos = tempContext.catalogos.ToList();
                //ViewBag.Catalogo = productos;

                var productos = tempAdo.GetProductos();
                ViewBag.Catalogo = productos;
            }
            catch (Exception ex)
            {

                throw;
            }
       


            return View(lista);
            //return View("");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}