﻿using RegistroEstudiantes.Data;
using RegistroEstudiantes.Data.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RegistroEstudiante.Web.Controllers
{
    [Authorize]
    public class EstudiantesController : Controller
    {
        private readonly IEstudiantesService service;

        // GET: Estudiantes

        public EstudiantesController(IEstudiantesService service)
        {
            this.service = service;
        }

        public ActionResult Index()
        {
            var lista = service.Listar();

            return View(lista);
        }

        public ActionResult Details(int Id)
        {
            var estudiante = service.GetById(Id);

            if (estudiante == null)
            {
                return View("EstudianteNoEncontrado");
            }
            return View(estudiante);
        }

        public ActionResult EstudianteNoEncontrado()
        {
            return View();
        }

        void LLenarSexos()
        {
            var sexos = new List<SelectListItem>
            {
                new SelectListItem { Value = "X", Text = "Seleccione" },
                new SelectListItem { Text = "Femenino", Value = "F" },
                new SelectListItem { Value = "M", Text = "Masculino" }
            };

            ViewBag.Sexos = sexos;
        }

        [HttpGet]
        public ActionResult Create()
        {
            LLenarSexos();

            return View();
        }

        [HttpPost]
        public ActionResult Create(Estudiante estudiante)
        {
            LLenarSexos();

            if (estudiante.Sexo == "X")
            {
                ModelState.AddModelError("Sexo", "Favor especifique el sexo del estudiante");
            }

            if (ModelState.IsValid)
            {
                //service.Crear(estudiante);
                //service.CrearSp(estudiante);

                var viPrintAdo = new ViPrintAdo();

                viPrintAdo.InsertarEstudiante(estudiante);
                return RedirectToAction("Details", new { Id = estudiante.Id });
            }

            return View();
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            LLenarSexos();

            var estudiante = service.GetById(Id);

            return View(estudiante);
        }

        [HttpPost]
        public ActionResult Edit(Estudiante estudiante)
        {
            if (estudiante.Sexo == "X")
            {
                ModelState.AddModelError("Sexo", "Favor especifique el sexo del estudiante");
            }

            if (ModelState.IsValid)
            {
                service.Modificar(estudiante);
                TempData["Mensaje"] = "El estudiante fue modificado";
                return RedirectToAction("Details", new { Id = estudiante.Id });
            }

            return View();
        }

        [HttpGet]
        public ActionResult Delete(int Id)
        {
            var estudiante = service.GetById(Id);

            return View(estudiante);
        }

        [HttpPost]
        public ActionResult Delete(int Id, FormCollection form)
        {
            var estudiante = service.GetById(Id);

            service.Eliminar(estudiante);

            return RedirectToAction("Index");
        }
    }
}