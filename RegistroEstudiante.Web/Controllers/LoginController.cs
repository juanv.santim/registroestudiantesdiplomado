﻿using RegistroEstudiantes.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RegistroEstudiante.Web.Controllers
{
    public class LoginController : Controller
    {
        private RegistroEstudiantesContext db;

        public LoginController(RegistroEstudiantesContext db)
        {
            this.db = db;
        }

        // GET: Login
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Login(string usuario, string clave)
        {
            var passwordMD5 = ConvertStringtoMD5(clave);

            var usr = db.Usuarios.FirstOrDefault(u => u.NombreDeUsuario == usuario && u.Clave == passwordMD5);

            if (usr != null)
            {
                FormsAuthentication.SetAuthCookie(usuario, false);
                HttpCookie cookie = new HttpCookie("UsuarioId", usr.Id.ToString());
                Response.Cookies.Add(cookie);
                return RedirectToAction("Index", "Home");
            }
            else {
                TempData.Add("Mensaje", "Usuario y/o contrasena invalidos");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult CerrarSesion(FormCollection form)
        {
            Request.Cookies.Clear();
            FormsAuthentication.SignOut();

            return RedirectToAction("Index");
        }

        public static string ConvertStringtoMD5(string strword)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(strword);

            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }
    }
}