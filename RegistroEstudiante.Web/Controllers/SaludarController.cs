﻿using RegistroEstudiante.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RegistroEstudiante.Web.Controllers
{
    public class SaludarController : Controller
    {
        // GET: Saludar
        public ActionResult Index()
        {
            var saludarViewModel = new SaludarViewModel();
            saludarViewModel.MensajeSaludo = ConfigurationManager.AppSettings["MensajeSaludo"];

            saludarViewModel.Nombre = HttpContext.Request.QueryString["nombre"];
            saludarViewModel.Apellido = HttpContext.Request.QueryString["apellido"];

            return View(saludarViewModel);
        }
    }
}