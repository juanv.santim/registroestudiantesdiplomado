﻿using RegistroEstudiantes.Data;
using RegistroEstudiantes.Data.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace RegistroEstudiante.Web.Api
{
    [EnableCors("*", "*", "*")]
    public class EstudiantesController : ApiController
    {
        public IEstudiantesService service { get; }

        public EstudiantesController(IEstudiantesService service)
        {
            this.service = service;
        }

        public IEnumerable<Estudiante> Get()
        {
            var estudiantes = service.Listar();

            return estudiantes;
        }
    }
}
