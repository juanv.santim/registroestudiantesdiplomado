﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroEstudiante.Web.Models
{
    public class SaludarViewModel
    {
        public string MensajeSaludo { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
    }
}