﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using RegistroEstudiantes.Data.Context;
using RegistroEstudiantes.Data.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace RegistroEstudiante.Web
{
    public static class ContainerConfig
    {
        internal static void ResolveDependencies(HttpConfiguration httpConfiguration)
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterApiControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<RegistroEstudiantesEFService>().As<IEstudiantesService>().InstancePerRequest();
            builder.RegisterType<RegistroEstudiantesContext>().InstancePerRequest();

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            httpConfiguration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

        }
    }
}