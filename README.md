# RegistroEstudiantesDiplomado

Se requiere crear una aplicación web para el registros de estudiantes en los diferentes cursos diplomados de una prestigiosa universidad del país.

La aplicación debe permitir los siguiente:
	- Registro y consulta de los estudiantes.	
	- Registro y consulta de los diferentes cursos que la universidad ofrece.
	- Registrar estudiantes en un curso en específico.
	- Consultar los estudiantes que se encuentren registrados en un curso.
	- Retirar un estudiante de un curso.
